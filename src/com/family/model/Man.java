package com.family.model;

import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<Human.DayOfWeek, Human.Activity> schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    public Man() {
    }

    void repairCar(){
        System.out.println("Полагодити авто");
    }

   /* @Override
    public void greetPet() {
        System.out.println("Привіт, " + getFamily().getPet().getNickname() + ", як справи, друже?");
    }*/

    @Override
    public void greetPet() {
        super.greetPet();
    }
}
